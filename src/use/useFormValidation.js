import useValidators from "./useValidators.js"

export default function useFormValidation() {
  const { isEmpty, minLength, maxLength, isEmail } = useValidators()

  // name
  const validateNameField = fieldValue => {
    return isEmpty(fieldValue)
      ? "Field must not be empty"
      : minLength(fieldValue, 2)
      ? "Field must be more than 2 characters"
      : maxLength(fieldValue, 32)
      ? "Field must be less than 32 characters"
      : null
  }

  // password
  const validatePasswordField = fieldValue => {
    return isEmpty(fieldValue)
      ? "Field must not be empty"
      : minLength(fieldValue, 8)
      ? "Field must be more than 8 characters"
      : maxLength(fieldValue, 32)
      ? "Field must be less than 32 characters"
      : null
  }

  // confirm password
  const validateConfirmPasswordField = (fieldValue, rest) => {
    const currentPassword = Array.isArray(rest) ? rest[0] : rest
    return fieldValue !== currentPassword ? "Your password and confirmation password do not match" : null
  }

  // email
  const validateEmailField = fieldValue => {
    return isEmpty(fieldValue) ? "Field must not be empty" : isEmail(fieldValue) ? "Enter valid email" : null
  }

  // phone
  const validatePhoneField = (fieldValue, rest) => {
    const mask = Array.isArray(rest) ? rest[0] : rest
    const toE164Format = phone => phone.split("").filter(n => ![" ", "-", "(", ")"].includes(n)).length

    return toE164Format(fieldValue) !== toE164Format(mask) ? "Enter valid phone number" : null
  }

  // country
  const validateCountryField = fieldValue => {
    return isEmpty(fieldValue) ? "Сhoose a country" : null
  }

  const clearFieldError = field => {
    field.error = null
    field.isValid = true
  }

  const setFieldError = (field, errorText) => {
    field.error = errorText
    field.isValid = false
  }

  return {
    validateNameField,
    validateEmailField,
    validatePasswordField,
    validateConfirmPasswordField,
    validatePhoneField,
    validateCountryField,
    clearFieldError,
    setFieldError,
  }
}
