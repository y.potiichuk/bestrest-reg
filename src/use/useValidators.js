export default function useValidators() {
  const isEmpty = fieldValue => {
    return fieldValue == null || fieldValue.length <= 0
  }

  const minLength = (fieldValue, min) => {
    return fieldValue.length < min
  }

  const maxLength = (fieldValue, max) => {
    return fieldValue.length > max
  }

  const isEmail = fieldValue => {
    let re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return !re.test(fieldValue)
  }

  return { isEmpty, minLength, maxLength, isEmail }
}
