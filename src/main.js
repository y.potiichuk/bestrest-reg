import { createApp } from "vue"
import App from "./App.vue"
import PrimeVue from "primevue/config"

// styles
import "@/assets/styles.scss"
import "primevue/resources/primevue.min.css"

createApp(App).use(PrimeVue).mount("#app")
